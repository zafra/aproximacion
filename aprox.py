#!/usr/bin/env pythoh3

"""
Cálculo del número óptimo de árboles.
"""

"""
Programa creado por Alberto García Zafra.
"""

import sys

"""
Esta funcíon calcula la producción para un número determinado de árboles que se pasa como parámetro.
"""


def compute_trees(trees):
    total_reduction = (trees - base_trees) * reduction
    reduced_fruit = fruit_per_tree - total_reduction
    production = trees * reduced_fruit
    return production


"""
Esta función devuelve una lista, para un número de árboles comprendidos entre un máximo y un mínimo,
en la que cada elemento es una tupla de dos elementos: el número de árboles y su producción,
para calcular la producción hace uso de la función compute_trees.
"""


def compute_all(min_trees, max_trees):
    productions = []

    for trees in range(min_trees, max_trees + 1):
        production = compute_trees(trees)
        productions.append((trees, production))
    return productions


""" 
Esta función lee los los valores pasados por la línea de comandos,
que se guarda en las variables correspondientes.
También permite comprobar si se usan menos parámetros de los requeridos,
o si los valores pasados por línea de comandos no son enteros, para ello se capturan excepciones. 
Si se usan más valores de los requeridos también dará un error.
"""


def read_arguments():
    try:
        base_trees = int(sys.argv[1])
        fruit_per_tree = int(sys.argv[2])
        reduction = int(sys.argv[3])
        min = int(sys.argv[4])
        max = int(sys.argv[5])
    except IndexError:
        sys.exit("Usage: aprox.py <base_trees> <fruit_per_tree> <reduction> <min> <max>")
    except ValueError:
        sys.exit("All arguments must be integers")

    if len(sys.argv) > 6:
        sys.exit("Usage: aprox.py <base_trees> <fruit_per_tree> <reduction> <min> <max>")

    return base_trees, fruit_per_tree, reduction, min, max


"""
Función principal que permite comprobar el número de árboles óptimo para una mayor producción.
"""


def main():
    global base_trees
    global fruit_per_tree
    global reduction

    best_production = 0
    best_trees = 0
    base_trees, fruit_per_tree, reduction, min, max = read_arguments()

    productions = compute_all(min, max)

    for treeproduct in productions:
        trees = treeproduct[0]
        production = treeproduct[1]
        print(trees, production)
        if production > best_production:
            best_trees = trees
            best_production = production
    print(f"Best production: {best_production}, for {best_trees} trees")


if __name__ == '__main__':
    main()
