#!/usr/bin/env pythoh3

'''
Cálculo del número óptimo de árboles.

Producción de un campo con árboles frutales. Si hay base_tree
árboles, producen cada uno fruit_per_tree kilos de fruta cada uno.
Por cada árbol más que se planta, la producción de cada árbol
se reduce en reduction kilos de fruta.
Calcular el número óptimo de árboles entre dos números dados.
'''

base_trees = 25
fruit_per_tree = 400
reduction = 10

def compute(trees):

    total_reduction = (trees - base_trees) * reduction
    reduced_fruit = fruit_per_tree - total_reduction
    production = trees * reduced_fruit
    return production

def main():

    (min, max) = (25, 45)
    best_production = 0
    best_trees = 0
    for trees in range(min, max+1):
        production = compute(trees)
        if production > best_production:
            best_trees = trees
            best_production = production
        print(trees, production)
    print(f"Best production: {best_production}, for {best_trees} trees")
    
if __name__ == '__main__':
    main()
